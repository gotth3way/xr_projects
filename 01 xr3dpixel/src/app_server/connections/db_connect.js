////##### mongoose connection
var mongoose = require('mongoose');
var env = process.env.NODE_ENV || 'development';

//mongod --dbpath "L:\_www\_NODE\data\dbs\DB_gotth3way" --port 30030
//  mongoose.connect("mongod:://"+ db_user + ":" + db_user_password + "@" + db_url + ":" + db_port + "/" + db_name, function(...))

var db = require('../_configs/db'); //la configuracion de la bd en remoto y en local
////console.log(db);
//1ª forma de conectarse -------------------------------------------------------
//1ª forma de conectarse
//"mongodb://" + db.user + ":" + db.password + "@" + db.host + ":" + db.port + "/" + db.database

// ################ CONEXION QUE FUNCIONA PARA VPS EN PRODUCCION
if (env === "production") {
    var uri = "mongodb://" + db.user + ":" + db.password + db.host + ":" + db.port + "/" + db.database;
    mongoose.connect(
        uri, 
        { 
            useUnifiedTopology: true, 
            useNewUrlParser: true, 
            useCreateIndex: true 
        })
        .then(()=>{
            console.log(`connection to database established: ` + uri);
        })
        .catch(err=>{
            console.log(`db error ${err.message}`);
            process.exit(-1);
        });
} else {// development
    // ################ CONEXION QUE FUNCIONA PARA VPS EN PRODUCCION
    //1ª forma de conectarse -------------------------------------------------------
    //
    //2ª forma de conectarse -------------------------------------------------------
    // ################ CONEXION QUE FUNCIONA PARA LOCALHOST EN DEVELOPMENT cuidao el @ debe llevarlo en development, puesto que el host en production comienza por @...
    var uri = "mongodb://" + db.user + ":" + db.password + "@" + db.host + ":" + db.port + "/" + db.database;
    mongoose.connect(
        uri,
        { 
            useUnifiedTopology: true, 
            useNewUrlParser: true, 
            useCreateIndex: true 
        }        
    );
    var db_con = mongoose.connection;
    db_con.on('error', console.error.bind(console, 'connection error:'));
    db_con.once('open', function() {
        // we're connected!
        console.log('Connected to Database on port: ' + db.port);
    });
}
// ################ CONEXION QUE FUNCIONA PARA LOCALHOST EN DEVELOPMENT
//2ª forma de conectarse -------------------------------------------------------
//
//3ª forma de conectarse ------------------------------------------------------- I LIKE THIS ********
// var uri = "mongodb://" + db.host + ":" + db.port + "/" + db.database;
// var options = {
//     user: db.user,
//     pass: db.password //,
//         //"collection": "mysessions",
//         //"clear_interval": 3600,
//         //"auto_reconnect": true
// };
// mongoose.connect(uri, options, function(error) {
//     // Check error in initial connection. There is no 2nd param to the callback.
//     if (error) {
//         console.log("Error: " + error);
//         //return next(error);//control de errores handleError -- no podemos hacer esto pues no tenemos todavía el req, res, next
//         //podemos intentar arrancar el servicio y hacer un segundo intento de conexion. Pero de momento no nos meteremos en más jaleos
//         //Si se rompe la app, por culpa de la conexion a la BD, que deberías (pues es preferible romper el servicio que mostrar una app sin datos)
//         //La única solución es un script que se autoejecute cuando la BD deje de funcionar, hasta un cierto número de veces.
//         //Para más opciones de conexión: http://mongoosejs.com/docs/connections.html
//     } else {
//         console.log("Connected to Database");
//     }
// });
//3ª forma de conectarse ------------------------------------------------------- I LIKE THIS ********
//
//4ª forma con promises --------------------------------------------------------
//mongoose.connect(uri, options).then(
//  () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */ },
//  err => { /** handle initial connection error }
//);
//4ª forma con promises --------------------------------------------------------
//
module.exports = mongoose;

// ############ DEBERIAS USAR ESTA Y CONTEMPLAR LA POSIBILIDAD, DE QUE HAYA UN REFUSE
// ¡¡¡¡ LEE ESTO  !!!!!!
//esa ruta en development y en production sera exactamente igual a no ser que la base de datos este
//en otro servidor
//controla esta conexion con usuario y contraseña
//para ello create un usuario administrador y su contraseña, para que solo desde la
//aplicacion con contraseña se pueda hacer la conexion
