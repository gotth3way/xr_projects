/*app.js */
/*
 @author      Juan Luna
 @description un servidor para la app avr3ds y rutas con controllers, conexion a bd
              y modelos, db/DB_avr3ds
 @scripts
              "scripts": {
              "ng": "ng",
              "start": "ng serve ",
              "build": "ng build --prod",
              "test": "ng test",
              "lint": "ng lint",
              "e2e": "ng e2e",
              "server": "nodemon server.js"
    }
 @DB_conexion mongod –dbpath C:\Users\desar\Desktop\portatil\_mongodata\DBS   --port 30030
 @DB_name     DB_avr3ds
 */
// ################################### MODULOS REQUERIDOS -----------------------------------------------------
var express = require('express');           // express
var bodyParser = require('body-parser');    // integrado en express desde la version 4.16.2 para json
var ejs = require('ejs');                   // motor plantillas
var cors = require('cors');                 // seguridad headers
var path = require("path");                 // directorios
var fs = require('fs');                     // manejo de ficheros
var https = require('https');               // protocolo seguro SSL, necsita instalar certificado CSR
// ################################### MODULOS REQUERIDOS ----------------------------------------------------- FIN
//
// ################################### SSL CERTIFICATE
var credentials = {
    ca: fs.readFileSync(__dirname + "/app_server/_configs/ssl/avr3dstudio_com.ca-bundle", 'utf8'), //la certification authority o CA
    key: fs.readFileSync(__dirname + "/app_server/_configs/ssl/avr3dstudio_com.key", 'utf8'), //la clave SSL, que es el primer archivo que generamos ;)
    cert: fs.readFileSync(__dirname + "/app_server/_configs/ssl/avr3dstudio_com.crt", 'utf8') //el certificado
};
// ################################### SSL CERTIFICATE
//
// ################################### CONFIGURACION EXPRESS --------------------------------------------------
var app = express(); //controlador principal del servidor de node
app.set('env', process.env.NODE_ENV || 'development'); //por defecto este será el env
// variables que dependen del entorno produccion o desarrollo
var env = process.env.NODE_ENV || 'development';

var staticPath = {
    development: 'app',
    production: 'app_client'
};
// var staticFolder = "app_client"; // para carpeta statica?
var statusCache = true; // cache en el render 'production'
if (app.get('env') === 'development') {
    statusCache = false;
    // var staticFolder = "app";
}
// console.log(path.join(__dirname, 'app_server', 'views'));
// console.log(staticFolder);
// STATIC FOLDER
app.use(express.static(path.join(__dirname, staticPath[env])));
// app.use(express.static(path.join(__dirname, 'dist')));
// para body datos y json
app.use(bodyParser.urlencoded({
    extended: false
})); //el servidor aceptará datos por post
app.use(bodyParser.json()); //para que esos datos vayan en formato json
// ################################### CONFIGURACION EXPRESS -------------------------------------------------- FIN
//
// ################################### MOTOR PLANTILLAS: ejs --------------------------------------------------
// determinar la cache en produccion o desarrollo statusCache
// app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'ejs');
app.engine('html', ejs.renderFile);
app.set('view cache', statusCache);
// console.log(app.get('views'));
// NOTE: You should always cache templates in a production environment.
// Don't leave both of these to `false` in production!
// ################################### MOTOR PLANTILLAS: ejs ------------------------------------------------- FIN
//
// ################################### CONFIGURACION CABECERAS Y CORS -----------------------------------------
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
// ################################### CONFIGURACION CABECERAS Y CORS ----------------------------------------- FIN
// ################################### midlewares cors -------------------------------------------------------
// app.use(cors());
// ################################### midlewares cors ------------------------------------------------------- FIN
//
// ################################### RUTAS ----------------------------------------------------------------
// importacion de rutas // development
var mainRoutes = require("./app_server/routes/main.routes.js");
var userRoutes = require("./app_server/routes/user.routes.js");
var loginRoutes = require("./app_server/routes/login.routes.js");
var contactRoutes = require("./app_server/routes/contact.routes.js");
// #### resolver rutas !!ojo cuidao deben respetar el orden
app.use('/user', userRoutes);
app.use('/login', loginRoutes);
app.use('/api/contact', contactRoutes);
app.use('/', mainRoutes);
// ################################### RUTAS ---------------------------------------------------------------- FIN
//
// ################################### app.listen -----------------------------------------------------------
// servidor secure layer transport SSL https://www.avr3dstudio.com:3000
if (process.env.NODE_ENV == 'production') {
    app.set('port', process.env.PORT || 3000); // 55109 si no pones ninguno
    var Server = https.createServer(credentials, app)
        .listen(app.get('port'), function() {
            console.log("NODE_ENV: " + app.get('env'));
            console.log('Express server with SSL certificate listening in https://www.avr3dstudio.com:' + Server.address().port);
        });
}
// servidor local http://localhost:3000
else {
    app.set('port', process.env.PORT || 3000); // 55109 si no pones ninguno
    var Server = app.listen(app.get('port'), function() {
        console.log("NODE_ENV: " + app.get('env'));
        console.log('Express server listening in http://localhost:' + Server.address().port);
    });
}
// ################################### app.listen ----------------------------------------------------------- FIN
module.exports = Server; // https://localhost:3000
