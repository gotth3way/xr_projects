import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.less']
})
export class ExampleComponent implements OnInit {

  animationFrame;
  Demo: any;
  constructor() { }

  ngOnInit() {
    this.animationFrame = (function() {
      return  window.requestAnimationFrame       ||
        window.webkitRequestAnimationFrame ||
        // window.mozRequestAnimationFrame    ||
        function(callback) {
        window.setTimeout(callback, 1000 / 60);
      };
    })();


    this.Demo = {

      loaded: 0,
      drag: false,
      speed: 0.5,
      brake: 1,
      r: 0,

      images: [
        'http://cs617727.vk.me/v617727366/942f/DqbS0IRIATA.jpg',
        'http://cs617727.vk.me/v617727366/9436/Ig4ieHZXvNo.jpg',
        'http://cs617727.vk.me/v617727366/943d/g8xqn7S87kQ.jpg',
        'http://cs617727.vk.me/v617727366/9444/DfhvfFfTarY.jpg',
        'http://cs617727.vk.me/v617727366/944b/-McVeNNxf-A.jpg',
        'http://cs617727.vk.me/v617727366/9452/w1bBTnHANig.jpg'
      ],

      Down: function(e) {
        this.Demo.o = this.Demo.r;
        this.Demo.x = this.Demo.r + e.clientX;
        this.Demo.drag = true;
        this.Demo.time = new Date();
      },

      Move: function(e) {
        if (this.Demo.drag) {
          this.Demo.r = this.Demo.x - e.clientX;
          this.Demo.p.style.webkitTransform = 'rotateY(' + this.Demo.r * 180 / 400 + 'deg)';
          this.Demo.p.style.mozTransform = 'rotateY(' + this.Demo.r * 180 / 400 + 'deg)';
          this.Demo.p.style.transform = 'rotateY(' + this.Demo.r * 180 / 400 + 'deg)';
        }
      },

      Up: function() {
        if (this.Demo.drag) {
          const time = this.newDate() - this.Demo.time;
          const path = this.Demo.r - this.Demo.o;
          this.Demo.speed = path / time * 5;
          this.Demo.brake = 1.01;
          this.Demo.drag = false;
        }
      },

      Spin: function() {
        if (!this.Demo.drag) {
          this.Demo.r += this.Demo.speed;
          this.Demo.speed /= this.Demo.brake;
          this.Demo.p.style.webkitTransform = 'rotateY(' + this.Demo.r * 180 / 400 + 'deg)';
          this.Demo.p.style.mozTransform = 'rotateY(' + this.Demo.r * 180 / 400 + 'deg)';
          this.Demo.p.style.transform = 'rotateY(' + this.Demo.r * 180 / 400 + 'deg)';
        }
        this.animationFrame(this.Demo.Spin);

      },

      Bind: function() {
        this.Demo.e.addEventListener('mousedown', this.Demo.Down, false);
        this.Demo.e.addEventListener('mousemove', this.Demo.Move, false);
        this.Demo.e.addEventListener('mouseup', this.Demo.Up, false);
        this.Demo.e.addEventListener('mouseleave', this.Demo.Up, false);
        this.animationFrame(this.Demo.Spin);
      },

      Progress: function() {
        this.Demo.loaded++;
        this.Demo.l.style.width = this.Demo.loaded / this.Demo.images.length * 100 + '%';
        if (this.Demo.loaded === this.Demo.images.length) {
          this.Demo.l.style.opacity = 0;
          this.Demo.p.style.opacity = 1;
          this.Demo.Bind();
        }
      },

      Load: function() {
        for (let i = 0; i < this.Demo.images.length; i++) {
          const img = new Image();
          img.addEventListener('load', this.Demo.Progress, false);
          img.src = this.Demo.images[i];
        }
      },

      Init: function() {
        this.Demo.e = document.getElementById('this.demo');
        this.Demo.p = document.getElementById('panorama');
        this.Demo.l = document.getElementById('loader');
        this.Demo.Load();
      }
    };



    document.addEventListener('DOMContentLoaded', this.Demo.Init, false);
  }


  private newDate() {
    return new Date();
  }
}
