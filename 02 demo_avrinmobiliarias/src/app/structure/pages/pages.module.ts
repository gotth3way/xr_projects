import { NgModule } from '@angular/core';

// my modules
import { SharedModule } from '../shared/shared.module';
// rutas
import { PAGES_ROUTES } from './pages.routes';

// componentes
import { PagesComponent } from './pages.component';
// products
import { ProductsComponent } from './products/products.component';
  // import { MenuSliderComponent } from './products/menu-slider/menu-slider.component';
  // import { MainComponent } from './products/main/main.component';
  // import { AsideComponent } from './products/aside/aside.component';
import { BlogComponent } from './blog/blog.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
// material imports
import { MatMenuModule, MatToolbarModule, MatTabsModule, MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



// MenuSliderComponent,
// MainComponent,
// AsideComponent,


@NgModule({
  imports: [
    MatMenuModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    BrowserAnimationsModule,
    SharedModule,
    PAGES_ROUTES
  ],
  declarations: [
    ProductsComponent,
    PagesComponent,
    HomeComponent,
    BlogComponent,
    AboutComponent,
    ContactComponent,

  ],
  exports: [
    PagesComponent,
    HomeComponent,
    ProductsComponent,
    BlogComponent,
    AboutComponent,
    ContactComponent
  ]
})
export class PagesModule { }
