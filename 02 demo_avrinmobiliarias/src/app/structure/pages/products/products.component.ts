import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// operadores de js
import 'rxjs/add/operator/map';

// carga plugins
declare function carga_PLUGINS_PRODUCTS();

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.less']
})
export class ProductsComponent implements OnInit {

  // ###### importacion de datos de bd
  myData: Array<any>;
  url: string;
  constructor(private http: HttpClient) {
    this.url = 'https://jsonplaceholder.typicode.com/photos';
    this.http.get(this.url, { observe : 'response'})
      .map((data: any) => data)
      .subscribe(
        (data: any) => {
            this.myData = data.body;
            // console.log(this.myData);
        },
        err => console.log(err), // error
        () => console.log('getDataStatus Complete') // complete
    );
    // ###### importacion de datos de bd}
  }

  ngOnInit() {
    carga_PLUGINS_PRODUCTS();
  }
}

