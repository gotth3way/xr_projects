import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';

import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { BlogComponent } from './blog/blog.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { NotfoundComponent } from '../../notfound/notfound.component';





const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {path: 'home', component: HomeComponent },
      {path: 'products', component: ProductsComponent },
      {path: 'blog', component: BlogComponent },
      {path: 'about', component: AboutComponent },
      {path: 'contact', component: ContactComponent },
      {path: '', redirectTo: '/home', pathMatch: 'full' },
      {path: '**', component: NotfoundComponent },
    ]
  }
];

export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes);
