function init_TRIANGLE() {
    // console.log("begin triangle");
    document.onreadystatechange = () => {
        if (document.readyState === 'complete') {
            var Width = window.innerWidth;
            var Height = window.innerHeight;
            var   bl_01 = "#1b225e", bl_02 = '#007948',
                  db_01 = '#0e1410', db_02 = '#1E1E1E'
                  dg_01 = '#0e0e02', dg_02 = '#454545', dg_03='#202020',
                  lg_01 = '#596054',
                  dv_01 = '#10023d'
                  ;
            // var colors = [];
            var OBJ_bg = [{
                    ord: 0,
                    name: "default_values",
                    width: 600,
                    height: 400,
                    cell_size: 75,
                    variance: 0.75,
                    x_colors: 'random',
                    y_colors: 'match_x',
                    palette: Trianglify.colorbrewer,
                    color_space: 'lab',
                    color_function: false,
                    stroke_width: 1.51,
                    seed: null
                },
                {
                    ord: 1,
                    name: 'cbd-green',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.95,
                    seed: '1fkwe',
                    x_colors: 'random'
                },
                {
                    ord: 2,
                    name: 'Rainbow',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.88,
                    seed: 'srre9',
                    x_colors: 'random'
                },
                {
                    ord: 3,
                    name: 'Pink',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.88,
                    seed: 'fanta',
                    x_colors: 'random'
                },
                {
                    ord: 4,
                    name: 'Mars',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.88,
                    seed: 'Love4',
                    x_colors: 'random'
                },

                {
                    ord: 5,
                    name: 'randon',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.88,
                    x_colors: 'random'
                },
                {
                    ord: 6,
                    name: 'Greens',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.88,
                    x_colors: 'Greens',
                    y_colors: 'match_x',
                    palette: Trianglify.colorbrewer
                },
                {
                    ord: 7,
                    name: 'Purples',
                    width: Width,
                    height: Height,
                    cell_size: 75,
                    variance: 0.88,
                    x_colors: 'Purples',
                    y_colors: 'match_x',
                    palette: Trianglify.colorbrewer
                },
                {
                    ord: 8,
                    name: 'Blues',
                    cell_size: 25,
                    width: Width,
                    height: Height,
                    variance: 0.88,
                    x_colors: 'PuBu',
                    y_colors: 'match_x',
                    palette: Trianglify.colorbrewer
                },
                {
                    ord: 9,
                    name: 'Blues-troll',
                    cell_size: 35,
                    width: Width,
                    height: Height,
                    variance: 0.88,
                    x_colors: 'YlGnBu',
                    y_colors: 'match_x',
                    palette: Trianglify.colorbrewer
                },
                {
                  ord:10,
                  name: 'my_palete',
                  variance: "0.93",
                  cell_size: 75,
                  x_colors: [db_01, db_02, bl_01, bl_02]
                },
                {
                  ord: 11,
                  name: 'my_bg_header',
                  variance: "0.93",
                  cell_size: 35,
                  x_colors: [ dv_01, dg_03]
                }
            ]; // for see palete brewer visit https://bl.ocks.org/mbostock/5577023

            // Objetos a trianglificar
            // .boxed
            // var headerBg = document.getElementsByClassName('boxed')[0];
            // CargaTriangles(OBJ_bg[11], headerBg);
            // body
            var mainBg = document.body;
            CargaTriangles(OBJ_bg[10], mainBg);
            // RESIZE WINDOW
            window.addEventListener('resize', function() {
                // console.log("window resizing !!!", mainBg, headerBg);
                // CargaTriangles(OBJ_bg[8], headerBg);
                CargaTriangles(OBJ_bg[10], mainBg);
            }, false);
        }
    };

    var colorFunc = function(x, y) {
        return 'hsl(' + Math.floor(Math.abs(x * y) * 360) + ',80%,60%)';
    };

    function CargaTriangles(x, toElem) {

        var pattern = Trianglify({
            width: toElem.clientWidth || x.width,
            height: toElem.clientHeight || x.height,
            variance: x.variance || 0.75,
            seed: x.seed || null,
            color_function: x.color_function || false,
            x_colors: x.x_colors || null,
            y_colors: x.y_colors || null,
            cell_size: x.cell_size || 75,
            palette: x.palette || Trianglify.colorbrewer,
            color_space: x.color_space || 'lab',
            stroke_width: x.stroke_width || 1.51,
        });
        var pngUri = pattern.png();

        toElem.style.backgroundImage = "url(" + pngUri + ")";

    }
}
